<?php

namespace Visu;

use Exception;
use Mpdf\Mpdf;

/**
 * Classe representativa da camada de visualização do sistema.
 */
class View {

	/**
	 * Parâmetros a serem utilizados durante a renderização.
	 *
	 * @var array Array associativo chave => valor
	 */
	protected $params = [];

	/**
	 * Método construtor privado. Esta classe não é instanciável publicamente.
	 */
	public function __construct($params = []){
		$this->params = $params;
	}

	/**
	 * Retorna o valor de um parâmetro de renderização.
	 *
	 * @param string $key Nome do Parâmetro
	 * @param mixed  $default Valor padrão, caso não esteja configurado
	 * @return mixed Valor do parâmetro buscado 
	 */
	public function get($key, $default = null){
		return $this->params[$key] ?? $default;
	}

	/**
	 * Função de renderização do template. Deve ser obrigatoriamente sobrescrito.
	 *
	 * @param mixed[] $params Parâmetro usados durante a renderização
	 * @return void
	 */
	public function render(){
		throw new Exception("Render not implemented.");
	}

	/**
	 * Renderiza um PDF a partir do conteúdo a ser exibido.
	 *
	 * @param string $title Nome do arquivo gerado
	 * @param string $dest Destino do Documento gerado: I = Inline, S = String, D = Download e F = Arquivo
	 * @param callable $beforeWrite Função que modifica o objeto mpdf antes da renderização.
	 * @param callable $beforeOutput Função que modifica o objeto mpdf após renderização e antes da exportação.
	 * @return void
	 */
	public function renderPDF($title = "untitled.pdf", $dest = "I", $beforeWrite = null, $beforeOutput = null){

		$pdf = new Mpdf();

		ob_start();
		$this->render();
		$html = ob_get_clean();

		if ($beforeWrite) $beforeWrite($pdf);
		$pdf->WriteHTML($html);
		if ($beforeOutput) $beforeOutput($pdf);
        $pdf->Output($title, $dest);
	}

	/**
	 * Configura o cabeçalho, código e corpo da
	 * resposta para apresentação em formato JSON.
	 * 
	 * @param mixed $data Dados a serem convertidos para o formato JSON.
	 * @param int	$code Código HTTP da resposta
	 */
	public function renderJSON($code = 200, $mode = false){
		http_response_code($code);
		header("Content-Type: application/json");
		echo json_encode($this->params, $mode ||
			JSON_PRETTY_PRINT |
			JSON_UNESCAPED_UNICODE |
			JSON_FORCE_OBJECT |
			JSON_UNESCAPED_SLASHES |
			JSON_NUMERIC_CHECK);
	}

	/**
	 * Apresenta uma tela de erro, com o código HTTP no cabeçalho.
	 * 
	 * @param int $code Código HTTP a ser enviado.
 	 */
	public function renderError($code = 500){
		http_response_code($code);
		$this->render();
	}

	/**
	 * Apresenta um arquivo em formato binário.
	 * @param string $contents Conteúdo a ser apresentado.
	 * @param string $filename Nome do arquivo para download.
	 */
	public static function raw($file, $filename = ""){
		
		$contents = file_get_contents($file);
		$filename = $filename? $filename : basename($file);
		$size = strlen($contents);

		header("Content-Type: application/octet-stream");
		header("Content-disposition: inline; filename=$filename");
		header("Content-Length: $size");
		
		echo $contents;
	}
}