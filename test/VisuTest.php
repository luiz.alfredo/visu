<?php
declare(strict_types=1);

namespace Test;

use Test\Views\Template;
use PHPUnit\Framework\TestCase;

final class VisuTest extends TestCase
{

    public function testViewWithParams(): void
    {
        $view = new Template(['msg' => 'Hello World']);
        $view->render();

        $this->expectOutputString("Hello World");
    }

    public function testViewWithEmptyParams(): void
    {
        $view = new Template();
        $view->render();
        
        $this->expectOutputString("Empty Msg");
    }


    /**
     * @runInSeparateProcess
     */
    public function testJSONRender(): void
    {
        $data = ["param" => "value"];
        $view = new Template($data);
       
        ob_start();
        $view->renderJSON();
        $json = ob_get_clean();

        $this->assertJsonStringEqualsJsonString(
            $json,
            json_encode($data)
        );
    }

    /**
     * @runInSeparateProcess
     */
    public function testViewPDF(): void
    {
        $filename = "test/file.pdf";
        @unlink($filename);
        
        $view = new Template();
        $view->renderPDF("test/file.pdf", "F");

        $this->assertFileExists($filename);
        $this->assertTrue(unlink($filename));
    }

}